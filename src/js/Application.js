import EventEmitter from "eventemitter3";

export default class Application extends EventEmitter {
  static get events() {
    return {
      READY: "ready",
    };
  }

  constructor() {
    super();
    this.emojis = ["🐒", "🦍", "🦧"];
    this.banana = "🍌";
    this.emit(Application.events.READY);
  }

  setEmojis(emojis) {
    this.emojis = emojis;

    const div = document.querySelector("#setEmojis");

    this.p = document.createElement("p");

    div.appendChild(this.p);

    this.p.textContent = this.emojis.join(",");

  }

  addBananas() {
    const feededMonkeys = this.emojis.map((emoji) => {
      return emoji + "🍌"
    
    });
    
    this.p.textContent = feededMonkeys.join(" ");
  }};
